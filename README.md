# README

This is a Contact List CRUD project as an example on how to built applications using TDD, Unit Testing, E2E testing and AngularJS.

You can read this file on http://dillinger.io/

If you have any issue to run the project, please contact me under [henriquecholo@gmail.com], ping me on Skype: [henriquecholo] or call me [+55 31 9469 9999].


## Pre Requisits
  - Ruby installed (required for Compass)
  - Node Installed

## Technical Details

This project was built using the following technologies:

  - Yeoman
  - HTML5
  - CSS3
  - AngularJS
  - SASS
  - Compass
  - Bootstrap
  - Grunt
  - Bower
  - Karma
  - Jasmine
  - Protractor
  - Node
  - Translate (English and French)

## Installation (to run only once)

FIrst you need to install RUby and Node. After, install compass, grunt and bower globally:

```sh
$ gem install compass
$ npm install -g grunt-cli
$ npm install -g bower
```
Now you can enter on the project folder, where you have package.json and bower.json and install the project (get the dependencies):

```sh
$ npm install
```

Your computer password will be required to install the compass gem

```sh
$ bower install
```

This will install everything you need to run the project, even the Compass that is installed by ```gem install compass```

After, run just once to update the webdriver:

```sh
$ npm test
```

## Running the project

To run the project you can execute inside the CRUD folder (which contains GruntFile.js):
```sh
$ grunt serve
```

For the E2E and Unit Tests:
```sh
$ grunt test
```

to generate a deployment version (will create a dist folder):
```sh
$ grunt build
```

## Comments

- I've built the project in two languages: English and French. In order to run on both languages, you just need to enter the file 'app/scripts/services/constants/language.js' and change the line 16:

```
  .constant('language', 'en_CA');   // 'fr_CA'
```

- I know that we can put the Edit part together in the same view, but i wanted to show more features and i believe having a edit view is more scalable (we can add more inputs).

- The Edit page contain a weird id number on the URL (i.e. contacts/-Jfnrr2lD7TSPntPRD5r/edit). This happens because Firebase automatically generates random numbers to solve concurrent implications.

- It is already performing three-way data-binding, however, we would only see if the views were shown simultaneously.
