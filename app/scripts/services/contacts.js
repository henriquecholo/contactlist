'use strict';

/**
 * @ngdoc service
 * @name contactListApp.contacts
 * @description
 * # contacts
 * Factory in the contactListApp.
 */
angular.module('contactListApp')
  .factory('Contacts', function (FIREBASEURL, $resource, $firebase, $window) {
    var ref = new $window.Firebase(FIREBASEURL + '/contacts'),
      contacts = $firebase(ref),
      Contact = {
        all: contacts,
        create: function (contact) {
          return contacts.$push(contact).then(function (ref) {
            return ref.key();
          });
        },
        edit: function (contact) {
          return contact.$save().then(function (ref) {
            return ref;
          });
        },
        get: function (contactId) {
          var singleRef = new $window.Firebase(FIREBASEURL + '/contacts/' + contactId);
          return $firebase(singleRef).$asObject();
        },
        delete: function (contact) {
          contacts.$remove(contact.$id).then(function (ref) {
            return ref;
          });
        }
      };

    return Contact;
  });
