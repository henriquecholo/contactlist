'use strict';

/**
 * @ngdoc service
 * @name contactListApp.language
 * @description
 * # language
 * Constant in the contactListApp.
 *
 * Options:
 *    en_CA
 *    fr_CA
 *
 */
angular.module('contactListApp')
  .constant('language', 'en_CA');
