'use strict';

/**
 * @ngdoc service
 * @name contactListApp.FIREBASEURL
 * @description
 * # FIREBASEURL
 * Constant in the contactListApp.
 */
angular.module('contactListApp')
  .constant('FIREBASEURL', 'https://flickering-fire-1311.firebaseio.com');
