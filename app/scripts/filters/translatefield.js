'use strict';

/**
 * @ngdoc filter
 * @name contactListApp.filter:translateField
 * @function
 * @description
 * # translateField
 * Filter in the contactListApp.
 */
angular.module('contactListApp')
  .filter('translateField', function ($translate) {
    return function (input) {
      return $translate.instant(input.toUpperCase());
    };
  });
