'use strict';

/**
 * @ngdoc filter
 * @name contactListApp.filter:phone
 * @function
 * @description
 * # phone
 * Filter in the contactListApp.
 */
angular.module('contactListApp')
  .filter('phone', function () {
    return function (input) {
      if(input !== undefined) {
        var inputString = input.toString();
        return '(' + inputString.substr(0,3) + ') ' + inputString.substr(3);
      }
      return input;
    };
  });
