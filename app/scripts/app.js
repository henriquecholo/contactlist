'use strict';

/**
 * @ngdoc overview
 * @name contactListApp
 * @description
 * # contactListApp
 *
 * Main module of the application.
 */
angular
  .module('contactListApp', [
    'ngAnimate',
    'ngResource',
    'ngRoute',
    'ngTouch',
    'trNgGrid',
    'dialogs.main',
    'ui.bootstrap',
    'pascalprecht.translate',
    'firebase'
  ])
  .config(function ($routeProvider, $translateProvider, language) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/contacts/:id/edit', {
        templateUrl: 'views/contacts.html',
        controller: 'ContactsCtrl'
      })
      .when('/contacts/new', {
        templateUrl: 'views/contacts.html',
        controller: 'ContactsCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });

    $translateProvider.translations('en_CA', {
      'MAIN_APPLICATION': 'Main Application',
      'CONTACT_LIST': 'Contact List',
      'CONFIRMATION': 'Confirmation',
      'REMOVE_CONTACT': 'Do you really want to delete the contact?',
      'LANGUAGE': 'Language',
      'NAME': 'Name',
      'ADDRESS': 'Address',
      'PHONE': 'Phone',
      'DIALOGS_YES': 'Yes',
      'DIALOGS_NO': 'No',
      'ADD_NEW_CONTACT': 'Add New Contact',
      'SUBMIT': 'Submit',
      'VAL_REQUIRED_NAME': 'The name is required.',
      'VAL_REQUIRED_ADDRESS': 'The address is required.',
      'VAL_REQUIRED_PHONE': 'The phone is required.',
      'CONTACT': 'Contact'
    });
    $translateProvider.translations('fr_CA', {
      'Search': 'Recherche',
      'First Page': 'Première Page',
      'Last Page': 'Dernière Page',
      'Sort': 'Ordre',
      'No items to display': 'Aucun élément à afficher',
      'displayed': 'affichée',
      'in total': 'en tout',
      'MAIN_APPLICATION': 'Application Principale',
      'CONTACT_LIST': 'Liste de contacts',
      'CONFIRMATION': 'Confirmation',
      'REMOVE_CONTACT': 'Voulez-vous vraiment supprimer le contact?',
      'LANGUAGE': 'Langue',
      'NAME': 'Nom',
      'ADDRESS': 'Adresse',
      'PHONE': 'Téléphone',
      'DIALOGS_YES': 'Oui',
      'DIALOGS_NO': 'Non',
      'ADD_NEW_CONTACT': 'Ajouter un contacter',
      'SUBMIT': 'Soumettre',
      'VAL_REQUIRED_NAME': 'Le nom est requis.',
      'VAL_REQUIRED_ADDRESS': 'L\'adresse est nécessaire.',
      'VAL_REQUIRED_PHONE': 'Le téléphone est nécessaire.',
      'CONTACT': 'Contacter'
    });

    $translateProvider.use(language);

  });
