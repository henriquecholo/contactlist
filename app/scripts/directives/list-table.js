'use strict';

/**
 * @ngdoc directive
 * @name contactListApp.directive:listTable
 * @description
 * # listTable
 */
angular.module('contactListApp')
  .directive('listTable', function ($translate, dialogs, $location) {
    return {
      templateUrl: '../../views/directives/list-table.html',
      restrict: 'E',
      scope: {
        list: '=list'
      },
      link: function postLink(scope) {
        scope.edit = function(contact) {
          $location.path('/contacts/' + contact.$id + '/edit');
        };

        scope.remove = function (contact) {
          var dialog = dialogs.confirm($translate.instant('CONFIRMATION'), $translate.instant('REMOVE_CONTACT'));
          dialog.result.then(function(){
            scope.list.delete(contact);
          },function(){
          });
        };
      }
    };
  });
