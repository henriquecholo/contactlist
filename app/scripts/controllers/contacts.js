'use strict';

/**
 * @ngdoc function
 * @name contactListApp.controller:ContactsCtrl
 * @description
 * # ContactsCtrl
 * Controller of the contactListApp
 */
angular.module('contactListApp')
  .controller('ContactsCtrl', function ($scope, $location, $routeParams, Contacts) {

    $scope.isEdition = $routeParams.id !== undefined;
    $scope.contact = $scope.isEdition ? Contacts.get($routeParams.id) : undefined;

    $scope.save = function(contact) {
      if($scope.isEdition) {
        Contacts.edit(contact).then(function() {
          $scope.return();
        });
      } else {
        Contacts.create(contact).then(function() {
          $scope.return();
        });
      }
    };
    $scope.return = function() {
      $location.path('/');
    };
  });
