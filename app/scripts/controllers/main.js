'use strict';

/**
 * @ngdoc function
 * @name contactListApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the contactListApp
 */
angular.module('contactListApp')
  .controller('MainCtrl', function ($scope, Contacts, $location) {

    $scope.contacts = Contacts;

    $scope.new = function() {
      $location.path('/contacts/new');
    };

  });
