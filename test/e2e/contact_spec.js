'use strict';

describe('Contact View', function() {

  beforeEach(function () {
    browser.get('http://localhost:9001/#/contacts/new');
  });

  it('should redirect to /contact view when location hash/fragment is ', function() {
      expect(browser.getLocationAbsUrl()).toMatch("/contacts/new");
  });

  it('should add an item and rollback to the main page', function() {
    var name = element(by.model('contact.name'));
    var address = element(by.model('contact.address'));
    var phone = element(by.model('contact.phone'));

    name.sendKeys('Henrique');
    address.sendKeys('Raul Hanriot');
    phone.sendKeys('3194699999');

    var submitButton = element(by.id('submitButton'));
    submitButton.click();
    expect(browser.getLocationAbsUrl()).toMatch("/");
  });

});
