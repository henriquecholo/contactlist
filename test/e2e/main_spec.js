'use strict';

describe('Main Homepage', function() {

  beforeEach(function () {
    browser.get('http://localhost:9001');
  });

  it('should automatically redirect to / when location hash/fragment is empty', function() {
    expect(browser.getLocationAbsUrl()).toMatch("/");

  });

  it('should display the title', function () {
    expect(browser.getTitle()).toEqual('Angular App');
  });

  it('should redirect to the new contact view once button is clicked', function () {
    var addButton = element(by.id('addButton'));
    addButton.click();
    expect(browser.getLocationAbsUrl()).toMatch("/contacts/new");
  });
});
