'use strict';

describe('Directive: listTable', function () {

  // load the directive's module
  beforeEach(module('contactListApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope, $httpBackend) {
    scope = $rootScope.$new();

    element = $httpBackend.whenGET('views/directives/list-table.html');

    scope.$digest();
  }));

  it('should have a table', function () {
    expect(angular.element(element).find('table')).toBeDefined();
  });
});
