'use strict';

describe('Service: Contacts', function () {

  // load the service's module
  beforeEach(module('contactListApp'));

  // instantiate service
  var contacts, timeout;
  beforeEach(inject(function (Contacts, $timeout) {
    contacts = Contacts;
    timeout = $timeout;
  }));

  it('should have a truthy list', inject(function() {
    expect(contacts.all).toBeTruthy();
  }));

  it('should add two items on the list', inject(function() {
    contacts.create({
      "name": "Henrique",
      "address": "RaulHanriot",
      "phone": "94699999"
    });

    contacts.create({
      "name": "Henrique2",
      "address": "RaulHanriot2",
      "phone": "94699999"
    });

    var array = contacts.all.$asArray();
    timeout.flush(); // Forcing insertion for test

    expect(array.length).toBe(2);

  }));

  it('should create, get and edit an item on the list', inject(function() {
    contacts.create({
      "name": "Henrique",
      "address": "RaulHanriot",
      "phone": "3194699999"
    });

    var array = contacts.all.$asArray();
    timeout.flush();
    var key = array.length - 1;

    expect(array[key].name).toBe("Henrique");
    expect(array[key].address).toBe("RaulHanriot");
    expect(array[key].phone).toBe("3194699999");

    var contact = contacts.get(key);

    expect(contact).toBeTruthy();

    contact.name = "Henrique Mello Cholodovskis Filho";
    contact.address = "Raul Hanriot Street";
    contact.phone = "553194699999";

    contacts.edit(contact);

    timeout.flush();

    expect(contact.name).toBe("Henrique Mello Cholodovskis Filho");
    expect(contact.address).toBe("Raul Hanriot Street");
    expect(contact.phone).toBe("553194699999");

  }));

  it('should create and remove an item on the list', inject(function() {
    var firstArray = contacts.all.$asArray();

    contacts.create({
      "name": "Henrique",
      "address": "RaulHanriot",
      "phone": "3194699999"
    });

    timeout.flush();

    var contact = contacts.get(firstArray.length - 1);
    contacts.delete(contact);

    var secondArray = contacts.all.$asArray();
    timeout.flush();
    expect(secondArray.length).toBe(firstArray.length);

  }));

});
