'use strict';

describe('Filter: phone', function () {

  // load the filter's module
  beforeEach(module('contactListApp'));

  // initialize a new instance of the filter before each test
  var phone;
  beforeEach(inject(function ($filter) {
    phone = $filter('phone');
  }));

  it('should return the phone number formatted', function () {
    expect(phone(1234567)).toBe('(123) 4567');
  });

});
