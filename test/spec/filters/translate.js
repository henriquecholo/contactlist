'use strict';

describe('Filter: translateField', function () {

  // load the filter's module
  beforeEach(module('contactListApp'));

  // initialize a new instance of the filter before each test
  var filter, translate;
  beforeEach(inject(function ($filter, $translate) {
    filter = $filter('translateField');
    translate = $translate;
  }));

  it('should print in English:"', function () {
    translate.use('en_CA');
    expect(filter('MAIN_APPLICATION')).toBe('Main Application');
  });

  it('should print in French:"', function () {
    translate.use('fr_CA');
    expect(filter('MAIN_APPLICATION')).toBe('Application Principale');
  });

});
