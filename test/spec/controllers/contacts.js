'use strict';

describe('Controller: ContactsCtrl', function () {

  // load the controller's module
  beforeEach(module('contactListApp'));

  var ContactsCtrl,
    scope, routeParams;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, $routeParams) {
    scope = $rootScope.$new();
    ContactsCtrl = $controller('ContactsCtrl', {
      $scope: scope
    });
    routeParams = $routeParams;
  }));

  it('should have the model contact only if is edition', function () {
    if(routeParams.id)
      expect(scope.contact).toBeDefined();
    else
      expect(scope.contact).toBe(undefined);
  });
});
